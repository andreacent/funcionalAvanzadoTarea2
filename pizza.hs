import Control.Monad
import Control.Monad.Trans

data Want a = Want ((a -> Pizza) -> Pizza)
--recibo func devuelvo pizza
data Pizza = Eat (IO Pizza)
           | Combo Pizza Pizza
           | Happy

instance Show Pizza where
    show (Eat x)     = " :-P "
    show (Combo x y) = " combo(" ++ show x 
                                 ++ "," 
                                 ++ show y ++ ") "
    show Happy       = " :-D "

want :: Want a -> Pizza
want = \(Want f) -> f (const Happy)

happy :: Want a
happy = Want $ \_ -> Happy

nomnom :: IO a -> Want a
nomnom = \io -> Want $ \f -> (Eat (liftM f io))

combo :: Want a -> Want ()
combo = \a -> Want $ \k -> Combo (want a) (k ())

pana :: Want a -> Want a -> Want a
pana a b = Want $ \g -> want b
    where g = (\f -> want a)

pizzeria :: [ Pizza ] -> IO ()
pizzeria = foldl return (return ())

instance Monad Want where
    return x       = Want $ \f -> f x
    --(Want f) >>= g = Want $ \k -> f (\x -> (g x) k)
    (Want f) >>= g = Want $ \k -> f (\x -> let Want j = g x in j k)
                     --where Want j = g x
--m >>= f  = WTF $ \k -> fromWTF m (\x -> fromWTF (f x) k)

hambre :: Want ()
hambre = pana (ponle (topping 42)) 
              (pana (ponle (topping 69))
                    (pana (ponle (topping 17)) 
                          (ponle (topping 23) >> nomnom (putStrLn ""))))

tengo :: Want a -> IO ()
tengo x = pizzeria [want x]

topping :: Int -> String
topping 17 = "/nlmce"
topping 23 = "/y./p6"
topping 42 = "htptuc2"
topping 69 = "t:irofr"
 
ponle :: String -> Want ()
ponle xs = mapM_ (nomnom . putChar) xs