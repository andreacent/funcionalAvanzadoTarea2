\documentclass[11pt,fleqn]{article}

\usepackage{tikz}
\usepackage{multicol}
\usepackage{latexsym}
\usepackage{array}
\usepackage[english,spanish]{babel}
\usepackage{lmodern}
\usepackage{listings}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[colorlinks=true,urlcolor=blue]{hyperref}
\usepackage{xcolor}

\usepackage{algorithmic}
\usepackage{algorithm}

\usetikzlibrary{positioning,shapes,folding,positioning,shapes,trees}

\hypersetup{
  colorlinks=true,
  linkcolor=blue,
  urlcolor=blue
}

\definecolor{brown}{rgb}{0.7,0.2,0}
\definecolor{darkgreen}{rgb}{0,0.6,0.1}
\definecolor{darkgrey}{rgb}{0.4,0.4,0.4}
\definecolor{lightgrey}{rgb}{0.95,0.95,0.95}

\lstset{
   language=Haskell,
   gobble=2,
   frame=single,
   framerule=1pt,
   showstringspaces=false,
   basicstyle=\footnotesize\ttfamily,
   keywordstyle=\textbf,
   backgroundcolor=\color{lightgrey}
}

\long\def\ignore#1{}

\begin{document}

\title{CI4251 - Programación Funcional Avanzada \\ Tarea 2 - MiniLogo}

\author{Andrea Centeno\\ 10-10138}

\date{Mayo 24, 2016}

\maketitle

\pagebreak

\section*{MINILOGO\ldots}

\noindent
Para reescribir la librería \texttt{MiniLogo} fue necesario importar modulos adicionales y se removió \texttt{Control.Monad.State}.

\begin{lstlisting}

> {-# LANGUAGE DeriveDataTypeable #-}
> module LogoMachine where
> import Control.Monad.RWS
> import Data.Char (toLower)
> import Data.Sequence as DS
> import Data.Foldable as DF
> import qualified Data.Map     as DM
> import qualified Graphics.HGL as G

\end{lstlisting}
\noindent
Para el control de excepciones se importaron las siguientes:
\begin{lstlisting}

> import Control.Monad.Exception
> import qualified Control.Exception as CE (throw)
> import Data.Either (either)
> import Data.Typeable

\end{lstlisting}

\noindent
Los tipos de datos \texttt{LogoProgram}, \texttt{Direction}, \texttt{PenStatus}, 
\texttt{Figure} y la función \texttt{foldLP} no se modificaron.

\ignore{
\noindent
\texttt{LogoProgra} El tipo de datos @LogoProgram@ representa las instrucciones
  de máquina que es capaz de interpretar la Máquina Virtual Logo.
\begin{lstlisting}

> data LogoProgram = Fd Int                       -- ^ Avanzar N pasos.
>                  | Bk Int                       -- ^ Retroceder N pasos.
>                  | Rt Int                       -- ^ Girar N grados a derecha.
>                  | Lt Int                       -- ^ Girar N grados a izquierda.
>                  | Pu                           -- ^ Subir el lápiz.
>                  | Pd                           -- ^ Bajar el lápiz.
>                  | Pc String                    -- ^ Cambiar el color del lápiz.
>                  | Say String                   -- ^ Emitir un mensaje de texto.
>                  | Home                         -- ^ Regresar al origen.
>                  | Seq (DS.Seq LogoProgram)     -- ^ Secuencia de instrucciones.
>                  | Rep Int (DS.Seq LogoProgram) -- ^ Repetir N veces.
>                  deriving (Show, Eq)

\end{lstlisting}
\noindent
\texttt{foldLP} Catamorfismo (fold) sobre el tipo de datos de las instrucciones de la Máquina Virtual Logo, a ser aprovechado en la fase de conversión de instrucción hacia acciones monádicas
\begin{lstlisting}

> foldLP a b c d e f g h i j k inst =
>   case inst of 
>     (Fd n)    -> a n
>     (Bk n)    -> b n
>     (Rt n)    -> c n
>     (Lt n)    -> d n
>     Pu        -> e
>     Pd        -> f
>     (Pc s)    -> g s
>     (Say s)   -> h s
>     Home      -> i
>     (Seq l)   -> j (fmap (foldLP a b c d e f g h i j k) l)
>     (Rep n l) -> k n (fmap (foldLP a b c d e f g h i j k) l)

\end{lstlisting}


\begin{lstlisting}

> type Direction = Int
> data PenStatus = Up | Down
>                 deriving ( Show , Eq )
> 
> data Figure = Poly G.Color [ G.Point ]    -- poligono
>             | Text G.Color G.Point String -- texto
>             | Empty                       -- centinela
>             deriving ( Show , Eq )

\end{lstlisting}}

\noindent
El tipo \texttt{LogoS}, es el modelo de Estado.

\begin{lstlisting}

> data LogoS = LogoS {
>                        pos :: G.Point ,   -- (x , y )
>                        dir :: Direction , -- En grados
>                        pns :: PenStatus ,
>                        pnc :: G.Color 
>                    } deriving ( Show )

\end{lstlisting}

\noindent
El tipo \texttt{LogoR}, se usa para representar el ambiente de
«sólo lectura».

\begin{lstlisting}

> data LogoR = LogoR  { 
>                         tc :: DM.Map String G.Color
>                     } deriving (Show)

\end{lstlisting}

\noindent
Con \texttt{LogoE}, se manejan las excepciones. 

\begin{itemize}
\item
  \texttt{BadCombination}. Es un error fatal usar el color rojo 
  después del verde (o  viceversa).
\item
  \texttt{BadColorDirection}. Es un error fatal ir hacia la 
  izquierda dibujando con rojo.
\item
  \texttt{InvalidColor}. Es un error colocar un color que no 
  está en la tabla de colores.
\end{itemize}

\begin{lstlisting}

> data LogoE = BadCombination G.Color G.Color
>            | BadColorDirection 
>            | InvalidColor String
>            deriving (Show,Typeable)
> 
> instance Exception LogoE

\end{lstlisting}

\noindent
La funcion \texttt{badCombination} evalua si la combinación es posible. 

\begin{lstlisting}

> badCombination :: G.Color -> G.Color -> Bool
> badCombination G.Red G.Green = True
> badCombination G.Green G.Red = True
> badCombination _ _ = False 

\end{lstlisting}

\noindent
Se creó el tipo \texttt{LogoRWS}, el cual es un monad combinado que aprovecha \texttt{RWS} y \texttt{Exception}. 
\noindent
Se colocó el transformador de RWS sobre Exception, ya que si ocurre un error, no interesa el estado de la simulación.

\begin{lstlisting}

> type LogoRWS = RWST LogoR 
>                     (DS.Seq Figure) 
>                     LogoS 
>                     (ExceptionT IO) ()

\end{lstlisting}

\noindent
Se acoplaron las funciones al nuevo tipo \texttt{LogoRWS}.

\noindent
\texttt{noop}: Transformación de estado que no hace nada
\begin{lstlisting}

> noop :: LogoRWS 
> noop = return ()

\end{lstlisting}

\noindent 
\texttt{pu}: Transformación de estado y acumulador para subir el lápiz 
\begin{lstlisting}

> pu :: LogoRWS
> pu = do 
>     (s, w) <- listen get
>     case pns s of
>         Up   -> put s
>         Down -> do  put $ s { pns = Up }
>                     case (DS.viewr w) of
>                        (ds :> Empty) -> tell ds
>                        _             -> tell w

\end{lstlisting}

\noindent
\texttt{pd}: Transformación de estado y acumulador para bajar el lápiz
\begin{lstlisting}

> pd :: LogoRWS
> pd = do 
>     (s, w) <- listen get
>     case pns s of
>         Down -> put s
>         Up   -> do  put $ s { pns = Down }
>                     tell (w |> Empty)

\end{lstlisting}

\noindent
\texttt{pc}: Transformación de estado y ambiente de «sólo lectura» para cambiar el color del lápiz
\begin{lstlisting}

> pc :: String -> LogoRWS
> pc str = do 
>     s <- get
>     r <- ask
>     let c = case (DM.lookup (map toLower str) $ tc r) of
>                 Just color -> color
>                 Nothing    -> CE.throw $ InvalidColor str
>     when (badCombination (pnc s) c) $ 
>          CE.throw $ BadCombination (pnc s) c
>     put $ s { pnc = c }

\end{lstlisting}

\noindent
\texttt{say}: Transformación de estado y acumulador para emitir mensaje de texto
\begin{lstlisting}

> say :: String -> LogoRWS
> say m = do
>     (s, w) <- listen get
>     case pns s of
>         Up   -> put s
>         Down -> case d of 
>                     (ds :> Empty) -> do put s
>                                         tell ( ds |> t )
>                     _             -> do put s
>                                         tell ( w |> t )
>                 where d = DS.viewr w
>                       t = Text (pnc s) (pos s) m

\end{lstlisting}

\noindent
\texttt{fd}: Transformación de estado y acumulador para avanzar n pasos
\begin{lstlisting}

> fd :: Int -> LogoRWS
> fd n = listen get >>= moveForward n

\end{lstlisting}

\noindent
\texttt{bk}: Transformación de estado y acumulador para retroceder n pasos
\begin{lstlisting}

> bk :: Int -> LogoRWS
> bk n = listen get >>= moveForward (negate n)

\end{lstlisting}

\noindent
\texttt{moveForward}: Función auxiliar para \texttt{fd} y \texttt{bk} encargada de calcular el desplazamiento en la dirección actual, posiblemente generando la geometría asociada.
\noindent
Si el movimiento es hacia la izquierda y está dibujando con rojo, se lanza la excepción \texttt{BadColorDirection}.
\begin{lstlisting}

> moveForward :: Int -> (LogoS, DS.Seq Figure) -> LogoRWS
> moveForward n (s,_) | pns s == Up = 
>     put $ s { pos = move (pos s) n (dir s) (pnc s)}
> moveForward n (s, w) = do
>     case (DS.viewr $ w) of 
>       (ds :> Empty)     -> tell ( ds |> t )
>       (ds :> Poly pc l) -> if (pc == cc) 
>                             then tell (ds |> Poly cc (np:l))
>                             else tell ( w |> t )
>       _                 -> tell ( w |> t )
>     put s { pos = np }
>     where cc = pnc s
>           cp = pos s
>           np = move cp n (dir s) (pnc s)
>           t  = Poly cc [ np, cp ]
> 
> move :: G.Point -> Int -> Direction -> G.Color -> G.Point
> move (x,y) n d c =
>     let direc  = (pi * (fromIntegral d)) / 180
>         nn     = fromIntegral n
>         nx     = x + (round (nn * (cos direc)))
>         ny     = y + (round (nn * (sin direc)))
>     in if(nx < x && c == G.Red && d > 90 && d < 270)
>                 then CE.throw $ BadColorDirection 
>                 else (nx,ny)

\end{lstlisting}

\noindent
\texttt{lt}: Transformación de estado para girar n grados a la izquierda
\begin{lstlisting}

> lt :: Int -> LogoRWS
> lt n = get >>= put . turnLeft n

\end{lstlisting}

\noindent
\texttt{rt}: Transformación de estado para girar n grados a la derecha
\begin{lstlisting}

> rt :: Int -> LogoRWS
> rt n = get >>= put . turnLeft (negate n)

\end{lstlisting}

\noindent
\texttt{turnLef}: Función auxiliar para lt y rt encargada de calcular
   la rotación en grados, manteniendo los valores entre 0 y 359.
\begin{lstlisting}

> turnLeft :: Int -> LogoS -> LogoS
> turnLeft n s = s { dir = (dir s + n) `mod` 360 }

\end{lstlisting}

\noindent
\texttt{home}: Transformación de estado y acumulador para regresar al estado inicial
\begin{lstlisting}

> home :: LogoRWS
> home = do put $ initialS
>           tell DS.empty 

\end{lstlisting}

\noindent
\texttt{goHome}: Transformación de estado para regresar al origen
\begin{lstlisting}

> goHome :: LogoRWS
> goHome = do s <- get
>             put $ s { pos = (0,0), dir = 90 }

\end{lstlisting}

\noindent
\texttt{repN}: Transformación de estado para repetir n veces
   una transformación de RWS particular.
\begin{lstlisting}

> repN :: Int -> LogoRWS -> LogoRWS
> repN 0 p = noop
> repN n p = p >> (repN (pred n) p)

\end{lstlisting}

\noindent
\texttt{monadicPlot}: Aplica un catamorfismo (fold) sobre la estructura
   de datos que representa un programa para la Máquina Virtual Logo,
   de manera que lo transforma en la secuencia de transformaciones de
   estado correspondientes a su interpretación.
\begin{lstlisting}

> monadicPlot :: LogoProgram -> LogoRWS
> monadicPlot = foldLP fd bk rt lt pu pd pc say home seq rep
>   where seq s   = if DS.null s then noop else DF.sequence_ s
>         rep n s = repN n (seq s)

\end{lstlisting}

\noindent
\texttt{initialS}: Estado inicial de la Máquina Virtual Logo
\begin{lstlisting}

> initialS :: LogoS
> initialS = LogoS {   
>                     pos = (0,0),
>                     dir = 90,
>                     pns = Up,
>                     pnc = G.White
>                  }

\end{lstlisting}

\noindent
\texttt{validColors}: es un valor constante que produce una tabla con los
  colores válidos de la Máquina Virtual Logo, utilizando cadenas
  alfanuméricas como clave de búsqueda.
\begin{lstlisting}

> validColors :: DM.Map String G.Color
> validColors = DM.fromList [
>                 ( "black",   G.Black   ),
>                 ( "red",     G.Red     ),
>                 ( "green",   G.Green   ),
>                 ( "blue",    G.Blue    ),
>                 ( "cyan",    G.Cyan    ),
>                 ( "magenta", G.Magenta ),
>                 ( "yellow",  G.Yellow  ),
>                 ( "white",   G.White   )
>               ]

\end{lstlisting}

\noindent
 La función \texttt{runLogoProgram} interpreta un programa escrito con
  las instrucciones de la Máquina Logo, produciendo la salida
  gráfica asociada.
\begin{lstlisting}

> runLogoProgram :: Int         -- Anchura de la ventana.
>                -> Int         -- Altura de la ventana.
>                -> String      -- Titulo para la ventana.
>                -> LogoProgram -- Instrucciones Maquina Logo.
>                -> IO ()
> runLogoProgram w h t p = 
>     G.runGraphics $ do
>         window <- G.openWindow t (w,h)
>         a <- runExceptionT  (execRWST 
>                               (monadicPlot p) 
>                               (LogoR validColors) 
>                               initialS
>                             )   
>         (_,wr) <- either throw return a
>         let (x0,y0) = origin w h
>         drawWindow (x0,y0) window wr DS.empty      
>         
> origin w h = (half w, half h)
>              where
>                 half i = round ((fromIntegral i)/2)

\end{lstlisting}

\noindent
\texttt{drawWindow}: Función auxiliar, la cual se encarga de producir la salida gráfica asociada a la Máquina Logo. Una vez presentado el dibujo, el programa espera por una tecla. Si se oprime la 'a', se «borra» el último paso del dibujo; si se oprime la 's' se «repete» el siguiente paso del dibujo; y si se oprime cualquier otra tecla se termina el programa.

\begin{lstlisting}

> drawWindow :: G.Point -> 
>               G.Window -> 
>               DS.Seq Figure -> 
>               DS.Seq Figure -> 
>               IO ()
> drawWindow (x0,y0) window w acc = do
>     G.clearWindow window
>     G.drawInWindow window $ G.overGraphics (
>         let f (Poly c p)   = G.withColor c $ G.polyline 
>                                              (map fix p)
>             f (Text c p s) = G.withColor c $ G.text (fix p) s
>             fix (x,y)      = (x0 + x, y0 - y)                
>         in DF.toList $ fmap (\x -> when (x /= Empty) (f x)) w
>         )
>     k <- G.getKey window
>     if G.isCharKey k 
>         then do let kc = G.keyToChar k  
>                 case kc of
>                   'a' -> when (not (DS.null w)) $
>                            let (ds :> t) = (DS.viewr $ w)
>                            in drawWindow (x0,y0) window ds 
>                                          ( acc |> t )
>                   's' -> when (not (DS.null acc)) $
>                            let (acc' :> t) = (DS.viewr $ acc) 
>                            in drawWindow (x0,y0) window 
>                                          ( w |> t ) acc'
>                   otherwise -> G.closeWindow window 
>                 drawWindow (x0,y0) window w acc
>         else G.closeWindow window  

\end{lstlisting}


\end{document}