module Main where

import LogoMachine
import qualified Data.Sequence as DS

star = Seq $ DS.fromList [
        Pd , Pc "yellow" ,
        Rep 36 $ DS.fromList [
          Rep 4 $ DS.fromList [
            Fd 200 , Rt 90
            ] , Rt 10
          ]
      ]

main = runLogoProgram 600 600 " MiniLogo " star