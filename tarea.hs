import Control.Applicative
import Prelude.Extras

data Bonus f a = Malus a
               | Bonus (f (Bonus f a))

instance (Show a, Show1 f) => Show (Bonus f a) where
  showsPrec p (Malus a) = showParen (p > 0) $ ("Malus " ++) . showsPrec  11 a
  showsPrec p (Bonus a) = showParen (p > 0) $ ("Bonus " ++) . showsPrec1 11 a

instance Functor f => Functor (Bonus f) where
    fmap f (Malus a) = Malus (f a)
    fmap f (Bonus a) = Bonus $ fmap (fmap f) a

instance Functor f => Applicative (Bonus f) where
    pure = Malus
    Malus f <*> Malus b = Malus $ f b
    Malus f <*> Bonus b = Bonus $ fmap f <$> b
    Bonus a <*> b = Bonus $ (<*> b) <$> a

instance Functor f => Monad (Bonus f) where
    return = pure
    Malus a >>= f = f a
    Bonus m >>= f = Bonus ((>>= f) <$> m)